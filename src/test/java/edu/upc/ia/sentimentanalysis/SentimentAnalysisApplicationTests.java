package edu.upc.ia.sentimentanalysis;

import edu.upc.ia.sentimentanalysis.util.GoogleTranslate;
import org.junit.Test;

import java.io.IOException;

public class SentimentAnalysisApplicationTests {

    @Test
    public void contextLoads() throws IOException {
        System.out.println(GoogleTranslate.translate("en", "Esto es una desgracia"));
    }

}
