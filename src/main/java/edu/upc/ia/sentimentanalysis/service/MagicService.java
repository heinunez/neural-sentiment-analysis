package edu.upc.ia.sentimentanalysis.service;

import edu.upc.ia.sentimentanalysis.SentimentAnalysisApplication;
import edu.upc.ia.sentimentanalysis.iterator.SentimentExampleIterator;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;

@Component
public class MagicService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private String datasetPath;
    private String wordVectorsPath;

    private MultiLayerNetwork neuralNetwork;
    private SentimentExampleIterator testDataset;

    public MagicService(
            @Value("${ia.dataset.directory.path}") String datasetPath,
            @Value("${ia.wvectors.path}") String wordVectorsPath) {
        this.datasetPath = datasetPath;
        this.wordVectorsPath = wordVectorsPath;
    }

    @PostConstruct
    protected void init() throws Exception {


        //number of examples in each minibatch
        int batchSize = 64;
        //size of the word vectors - 300 in the Google News model
        int vectorSize = 300;
        //epoch = fill pass of training data
        int numOfEpochs = 1;
        //truncate reviews with length (number of words) greater than this
        int truncateLength = 256;
        final int seed = 0;

        //we disable periodic garbage collection calls: 10.000 milliseconds
        Nd4j.getMemoryManager().setAutoGcWindow(10000);  //https://deeplearning4j.org/workspaces

        //Set up network configuration
        MultiLayerConfiguration networkConfiguration = new NeuralNetConfiguration.Builder()
                .seed(seed)
                //documentation discuss how to update the weights (and how to handle learning rate and momentum)
                .updater(new Adam.Builder().learningRate(2e-2).build())
                //use L2 regularization to avoid overfitting
                .l2(1e-5)
                //Xavier initialization helps to avoid vanishing gradients problem
                .weightInit(WeightInit.XAVIER)
                //maximizing the gradient helps to avoid exploding gradient problem
                .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue).gradientNormalizationThreshold(1.0)
                //.learningRate(2e-2)
                //going to be a bit slower but uses less memory ... tradeoff
                //trainingWorkspaceMode = forward propagation + backpropagation ... inferenceWorkspaceMode = just forward propagation
                .trainingWorkspaceMode(WorkspaceMode.SEPARATE).inferenceWorkspaceMode(WorkspaceMode.SEPARATE)
                .list()
                //this how we define the recurrent neural networks architecture: we use Long-Short Term Memory (LSTM)
                .layer(0, new GravesLSTM.Builder().nIn(vectorSize).nOut(256)
                        .activation(Activation.TANH).build())
                //of course we have the output layer with softmax activation function
                .layer(1, new RnnOutputLayer.Builder().activation(Activation.SOFTMAX)
                        //MCXENT loss function: multi-class cross entropy loss function
                        .lossFunction(LossFunctions.LossFunction.MCXENT).nIn(256).nOut(2).build())
                .pretrain(false).backprop(true).build();

        //let's create and initialize the recurrent neural network
        neuralNetwork = new MultiLayerNetwork(networkConfiguration);
        neuralNetwork.init();
        neuralNetwork.setListeners(new ScoreIterationListener(1));

        //DataSetIterators for training and testing
        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(new File(wordVectorsPath));
        SentimentExampleIterator trainingDataset = new SentimentExampleIterator(datasetPath, wordVectors, batchSize, truncateLength, true);
        testDataset = new SentimentExampleIterator(datasetPath, wordVectors, batchSize, truncateLength, false);

        //let's train the neural network
        for (int i = 0; i < numOfEpochs; i++) {
            neuralNetwork.fit(trainingDataset);
            trainingDataset.reset();
            neuralNetwork.evaluate(testDataset);
        }
    }

    public SentimentAnalysisApplication.Prediction doMagic(SentimentAnalysisApplication.Comment comment) {
        SentimentAnalysisApplication.Prediction prediction = new SentimentAnalysisApplication.Prediction();
        INDArray features = testDataset.loadFeaturesFromString(comment.getContent(), 256);
        logger.info("Feature representation of the sentence: {}", features);
        //prediction made by the neural network
        INDArray networkOutput = neuralNetwork.output(features);
        int numOfOutputs = networkOutput.size(2);
        //transform the prediction into probabilities
        INDArray sentimentProbabilities = networkOutput.get(NDArrayIndex.point(0), NDArrayIndex.all(), NDArrayIndex.point(numOfOutputs - 1));

        logger.info("Our review: {}", comment.getContent());
        logger.info("Result prediction: {}", networkOutput);

        prediction.setPositivePercent(sentimentProbabilities.getDouble(0));
        prediction.setNegativePercent(sentimentProbabilities.getDouble(1));
        return prediction;
    }
}
