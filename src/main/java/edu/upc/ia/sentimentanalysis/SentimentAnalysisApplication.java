package edu.upc.ia.sentimentanalysis;

import edu.upc.ia.sentimentanalysis.service.MagicService;
import edu.upc.ia.sentimentanalysis.util.GoogleTranslate;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;

@RestController
@SpringBootApplication
public class SentimentAnalysisApplication {

    @Autowired
    private MagicService magicService;

    @PostMapping("/comment")
    public Prediction comment(@RequestBody @Valid Comment comment) throws IOException {
        comment.setContent(GoogleTranslate.translate("es","en", comment.getContent()));
        return magicService.doMagic(comment);
    }

    @Data
    public static class Comment {
        @NotEmpty
        @Length(max = 256)
        private String content;
    }

    @Data
    public static class Prediction {
        private Double positivePercent;
        private Double negativePercent;
    }


    public static void main(String[] args) {

        //No limits!
        System.setProperty("org.bytedeco.javacpp.maxphysicalbytes", "0");
        System.setProperty("org.bytedeco.javacpp.maxbytes", "0");

        SpringApplication.run(SentimentAnalysisApplication.class, args);
    }

}
